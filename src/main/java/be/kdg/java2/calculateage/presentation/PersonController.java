package be.kdg.java2.calculateage.presentation;

import be.kdg.java2.calculateage.presentation.dto.PersonDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.Period;

@Controller
@RequestMapping("/personage")
public class PersonController {
    private static final Logger log = LoggerFactory.getLogger(PersonController.class);

    @GetMapping
    public String getPersonAgeForm(){
        log.debug("Getting the person age form..");
        return "personage";
    }

    @PostMapping
    public String calculatePersonAge(PersonDTO personDTO, Model model){
        log.debug("Getting the person age form..");
        model.addAttribute("person", personDTO.getName());
        model.addAttribute("age", Period.between(personDTO.getBirthday(), LocalDate.now()).getYears());
        return "personage";
    }
}
